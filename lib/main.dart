import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class FavoriteWidget extends StatefulWidget {
  @override
  FavoriteWidgetState createState() => new FavoriteWidgetState();
}


class FavoriteWidgetState extends State<FavoriteWidget> {
  bool isFavorite = false;
  int favoriteCount = 0;

  void switchFavorite() {
    setState
      (() {
      if (isFavorite) {
        isFavorite = false;
        favoriteCount -= 1;
      }
      else {
        isFavorite = true;
        favoriteCount += 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        padding: const EdgeInsets.all(30.0),
        child: new Row(
          children: [
            new Expanded(child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new Text("Niagara Falls",
                    style: new TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                new Text(
                  "Border of Ontario, Canada, and New York, United States",
                  style: new TextStyle(
                      color: Colors.grey, fontStyle: FontStyle.italic),
                )
              ],
            ),
            ),
            new IconButton(
              icon: (isFavorite
                  ? new Icon(Icons.star)
                  : new Icon(Icons.star_border)),
              color: Colors.blue,
              onPressed: switchFavorite,
            ),

            new Text('$favoriteCount'),
          ],
        ));
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = new Container(
      padding: const EdgeInsets.all(30.0),
      child: new Row(
        children: [
          new Expanded(child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: new Text("Niagara Falls",
                  style: new TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              new Text(
                "Border of Ontario, Canada, and New York, United States",
                style: new TextStyle(
                    color: Colors.grey, fontStyle: FontStyle.italic),
              )
            ],
          ),
          ),
          new Icon(Icons.star, color: Colors.blue),
          new Text("89"),
        ],
      ),
    );

    Column buildButtonColumn(IconData icon, String label) {
      Color color = Theme
          .of(context)
          .primaryColor;

      return new Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Icon(icon, color: color,),
          new Container(
              margin: const EdgeInsets.all(4.0),
              child: new Text(label, style: new TextStyle(
                  fontSize: 12.0, fontWeight: FontWeight.w300, color: color)
              )
          )
        ],
      );
    }

    Widget buttonSection = new Container(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            buildButtonColumn(Icons.favorite, "LIKE"),
            buildButtonColumn(Icons.share, "SHARE"),
          ],
        )
    );

    Widget descriptionSection = new Container(
        padding: const EdgeInsets.all(32.0),
        child: new Text(
          "Niagara Falls (/naɪˈæɡərə/) is the collective name for three waterfalls that straddle the international border between the Canadian province Ontario "
              "and the American state of New York. They form the southern end of the Niagara Gorge. From largest to smallest, the three waterfalls are the Horseshoe Falls, "
              "the American Falls and the Bridal Veil Falls. The Horseshoe Falls lies on the bordeof the United States and Canada with the American Falls entirely on "
              "the United States' side, separated by Goat Island. The smaller Bridal Veil Falls are also on the United States' side, separated from the American "
              "Falls by Luna Island. Located on the Niagara River, which drains Lake Erie into Lake Ontario, the combined falls form the highest flow rate of ay waterfall in "
              "North America that has a vertical drop of more than 165 feet (50 m). During peak daytime tourist hours, more than six million cubic feet (168,000 m3) "
              "of water goes over the crest of the falls every minute. Horseshoe Falls is the most powerful waterfall in North America, ",
          softWrap: true, textAlign: TextAlign.justify,)
    );


    return new MaterialApp(
      title: 'Landscape',
      theme: new ThemeData(primaryColor: Colors.blue),
      home: new Scaffold(
          appBar: new AppBar(
            title: new Text('Landscape'),
          ),
          body: new ListView(
            children: <Widget>[
              new Container(
                padding: new EdgeInsets.only(left: 5.0, right: 5.0),
                child: new Image.network(
                  'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/3Falls_Niagara.jpg/1920px-3Falls_Niagara.jpg',
                  height: 250.0,
                  fit: BoxFit.cover,
                ),
              ),
              //titleSection,
              new FavoriteWidget(),
              buttonSection,
              descriptionSection,
            ],
          )
      ),
    );
  }
}

